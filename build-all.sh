#!/bin/sh

# build everything.

components="gmp-utils primitives keyword"

for c in $components ; do
	cd $c && echo "building $c ..."
	make || echo "Error building $c"
	cd - > /dev/null
done

