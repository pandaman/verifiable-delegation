/* construction.h
 *
 * Header file for Construction 1 as described in
 * Verifiable Delegation of Computation over Large Datasets
 * by Siavosh Benabbas, Rosario Gennaro, Yevgeniy Vahlis
 *
 * */

#pragma once

#include "gmpxx.h"

namespace CAISS {
	struct key {
		mpz_class k0, k1;
	};
	// NOTE: is this useful?  or do we just have to type "G." more?  : )
	struct group {
		//const unsigned int d = 100;
		mpz_class g,p,q;
	};
	
	class C1_client // Construction 1 - Client
	{
	public:
		C1_client();
		key K;
		group G;
		// Generate group description (p, g, G) ←R G(1^n). k0, k1 ∈R Zp.
		// Output param = (m,p,g,G), K = (k0,k1)
		void key_gen(unsigned int n, group& G, key& K);
		// F_K(x): int x in {0, . . . , D = 2^m}; D is polynomial in n. Output
		// (g, k0, k1^x)
		void PRF_eval(mpz_class& rop, unsigned int x, const group& G,const key K);
		void CFEval(mpz_class& rop, const unsigned int x, const key K);
		// True if t_i == g^(c*y)*cfe_val
		bool test_pos(const mpz_class& t_i,const unsigned int x , const mpz_class& y);
		// How do I choose these values?
		uint16_t m; // smallest integer || d <= 2^m ; Let's us write d as an m-string
		uint16_t d; // degree of polynomial
	private:
		mpz_class c;
	};
	
	class C1_server // Construction 1 - Server
	{
	public:
		C1_server(const mpz_class& g, const mpz_class& p, const mpz_class& q, const uint16_t d);// will get g,p,q,d, and polynomians a_i up to d
		group G;
		uint16_t d; // degree of polynomial
		mpz_class* a;
		void get_y(mpz_class& y, const unsigned int x);
		void get_t(mpz_class& t_i, const unsigned int x);
		
	};
}
