/* Interface for polynomial keyword search. */
#include "alg-prf.h"
#include <vector>
using std::vector;

class wordSearch
{
public:
	wordSearch(){}
	wordSearch(char* file_name);
	~wordSearch() {};

	int keyGen(size_t sparam=1024);
	void hashWordsFromFile(char* file_name);
	mpz_class problemGen(mpz_class w);
	int compute(const mpz_class& x, mpz_class& y, mpz_class& t);
	bool verify(const mpz_class& x, mpz_class& y, mpz_class& t);
	bool searchString(const char* c, mpz_class& y, mpz_class& t);
	bool searchString(const string w, mpz_class& y, mpz_class& t);

private:
	algPRF F;
	mpz_class a;
	unsigned char* hmac_key;
	vector<mpz_class> C;
	vector<mpz_class> T; // group elements hiding the coeff's of R.
	/* NOTE: the prf group is global, so everyone can
	 * access the parameters q,g,p */
};
