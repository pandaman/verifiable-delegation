/* construction.cpp
 *
 * Implementation of Construction 1 as described in
 * Verifiable Delegation of Computation over Large Datasets
 * by Siavosh Benabbas, Rosario Gennaro, Yevgeniy Vahlis
 *
 * */

#include "construction1.h"
#include "gmp-utils.h"

#include <time.h>

// microsecond timer:
uint64_t clockGetTime_mu()
{
#ifdef LINUX
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (uint64_t)ts.tv_sec * 1000000LL + (uint64_t)ts.tv_nsec / 1000LL;
#else
	// add microsecond timer support for Mac OSX
	return 0;
#endif
}

namespace CAISS 
{
	C1_client::C1_client()
	{
		uint16_t nBits = 80;
		//pBits = 2*nBits
		//qBits = 1024*nBits/80 - 2*nBits
		m = 8;
		d = 130;
		key_gen(2*nBits,G,K);
		c = R.get_z_range(G.p);
	}
	void C1_client::key_gen(unsigned int n, group& G, key& K)
	{
		/* Use predefined values for g, p, and q.
		R.seed(clockGetTime_mu());// Implement better way to handle random nubmers
		genSafePrime(G.p, n);
		mpz_class t, p1 = G.p >> 1;
		
		do {
			G.g = R.get_z_range(G.p);
			POWERMOD(t,G.g,p1,G.p);
		} while (t != G.p-1);
		 */
		
		uint32_t gBytes[32] = {
			#include "g.txt"
		};
		uint32_t qBytes[32] = {
			#include "q.txt"
		};
		uint32_t pBytes[5] = {
			#include "p.txt"
		};
		mpz_t tmp;
		mpz_init(tmp);
		mpz_import(tmp,32,1,4,0,0,gBytes);
		G.g = mpz_class(tmp);
		mpz_import(tmp,32,1,4,0,0,qBytes);
		G.q = mpz_class(tmp);
		mpz_import(tmp,5,1,4,0,0,pBytes);
		G.p = mpz_class(tmp);
		mpz_clear(tmp);

		// Key
		K.k0 = R.get_z_range(G.p);
		K.k1 = R.get_z_range(G.p);
	}
	
	void C1_client::PRF_eval(mpz_class& rop, unsigned int x, const group& G,const key K)
	{
		mpz_class t,e;
		/* Compute e = k0*k1^x mod p-1.  We can reduce mod p-1 since
		 * all this is happening in an exponent. */
		mpz_class p1 = G.p - 1;
		POWERMOD_UI(t,K.k1,x,p1);
		MULMOD(e,K.k0,t,p1);
		POWERMOD(rop,G.g,e,G.p);
	}
	
	void C1_client::CFEval(mpz_class& rop,const unsigned int x, const key K)
	{
		mpz_class t,e;
		/* Compute e = k0*k1^x mod p-1.  We can reduce mod p-1 since
		 * all this is happening in an exponent. */
		mpz_class p1 = G.p - 1;
		POWERMOD_UI(t,K.k1,x,p1);
		MULMOD(e,K.k0,t,p1);
		POWERMOD(rop,G.g,e,G.p);
	}
	bool C1_client::test_pos(const mpz_class& t_i,const unsigned int x, const mpz_class& y)
	{
		mpz_class e,t,t_c,cfe_val;
		MULMOD(e, c, y, G.p); // e = c*y
		POWERMOD(t,G.g,e,G.p);// t = g^e mod p
		CFEval(cfe_val, x, K);// get cfe_val mod p
		MULMOD(t_c,cfe_val,t,G.q);// (cfe_val * g^e) mod q
		return t_i == t_c;
	}
	//	uint16_t cBits = 8; // coefficient bits
	//	mpz_class P_coef[d], R_coef[d]; //coefficient of polynomials P(.) and R(.)
	//	mpz_class t[d]; // group of elements of the form g^(a*c_i+r_i)
	//	mpz_class a = R.get_z_bits(cBits);
	//	unsigned long int exp;
	//	for (int i = 0; i<d; i++){
	//		P_coef[i]= R.get_z_bits(cBits); // c_i
	//		R_coef[i]= R.get_z_bits(cBits); // r_i
	//		MUL(x,a,P_coef[i]);
	//		exp = x.get_ui() + R_coef[i].get_ui();
	//		x += R_coef[i];
	//		POWERMOD(t[i],g,x,p); // Power Multiplication g^(a*c_i+r_i)
	//	}
	
	
	C1_server::C1_server(const mpz_class& g, const mpz_class& p, const mpz_class& q, const uint16_t d)
	{
		// shouldn't the constructor initialize the member variable a?
		// since it is a pointer and all?
		G.g = g;
		G.p = p;
		G.q = q;
	}
	void C1_server::get_y(mpz_class& y, const unsigned int x)
	{
		//y = SUM(a_i*x^i) %p
		mpz_class m,b = x;
		y = a[0];
		for (int i = 1; i<= d; i++)
		{
			MULMOD(m,a[i],b,G.p);
			y += m;
			b *= b;
		}
	}
	void C1_server::get_t(mpz_class& t_i, const unsigned int x)
	{
	}
}

