/* implementation of polynomial keyword search protocol. */
#include <cstdio>
#include <string.h>
#include <openssl/engine.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>
/* NOTE: gcc tells me hash_set is deprecated, and we should
 * instead use unordered_set. */
#include <ext/hash_set>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cassert>
#include <string>
using std::string;
using std::cout;
using std::cin;
using std::endl;
using namespace __gnu_cxx;
#include "word-search.h"
#include "gmp-utils.h"

#define DEBUG 0
#define P_BYTES 20

struct eqstr
{
	bool operator()(const char* s1, const char* s2) const
	{
		return strcmp(s1, s2) == 0;
	}
};
void lookup(const hash_set<const char*, hash<const char*>, eqstr>& Set,
            const char* word)
{
	hash_set<const char*, hash<const char*>, eqstr>::const_iterator it
    = Set.find(word);
	cout << word << ": "
	<< (it != Set.end() ? "present" : "not present")
	<< endl;
}

wordSearch::wordSearch(char* file_name)
{
	hashWordsFromFile(file_name);
	unsigned char k[] = "this is the key";
	wordSearch::hmac_key = &k[0];
	
};

int wordSearch::keyGen(size_t sparam)
{
	/* check: do we have a polynomial to delegate? */
	if (this->C.size() == 0) return -1;
	/* first run keygen for the algebraic PRF */
	this->F.keyGen();
	this->a = R.get_z_range(algPRF::p);
	/* now generate the elements g^{ac_i+r_i}, and store in T */
	mpz_class i1,i2,i3,t;
	this->T.clear();
	for (size_t i = 0; i < this->C.size(); i++) {
		i1 = this->F(i);
		MULMOD(i2,this->a,C[i],algPRF::p);
		POWERMOD(i3,algPRF::g,i2,algPRF::q);
		MULMOD(t,i1,i3,algPRF::q);
		this->T.push_back(t);
	}
	return 0;
}

void wordSearch::hashWordsFromFile(char* file_name){
	// Open File
	std::ifstream in_file;
	in_file.open(file_name);
	assert(in_file);	
	
	// Get words out of file, use hash set to avoid duplicates
	string word;
	hash_set<const char*, hash<const char*>, eqstr> wordSet;
	while ( in_file >> word ){
		int l = word.length();
		 if(word[l-1] > 122 || word[l-1] < 65 || word[l-1]%90 <7){
			 word.resize(l-1);
		 }
		cout<< wordSet.size()<< endl;
		wordSet.insert(reinterpret_cast<const char*>(word.c_str()));
		lookup(wordSet, reinterpret_cast<const char*>(word.c_str()));
	}
	in_file.close();
	
	//DEBUG STUFF
	lookup(wordSet, "mango");
	lookup(wordSet, "http://manybooks.net");
	hash_set<const char*,hash<const char*>, eqstr>::const_iterator i = wordSet.begin();
	for(;
	i!=wordSet.end();++i){
		cout << *i << endl;
	}
	// For some reasong this only finds the last word insterted in wordSet
	// Am I inserting words properly into the set?
	// Am I iterating throuhg the set properly?
	
	// END DEBUG STUFF
	
	
	// Computer HMAC for each word in word set
	unsigned int result_len = P_BYTES; // Should be less than size of p 
	unsigned char result[P_BYTES];
	HMAC_CTX ctx;
	ENGINE_load_builtin_engines();
	ENGINE_register_all_complete();
	HMAC_CTX_init(&ctx);
	mpz_class cache;
	vector<mpz_class> R;
	//for each word in the word set compute HMAC (roots of Polynomial)
		HMAC_Init_ex(&ctx, this->hmac_key, strlen((char*)this->hmac_key), EVP_sha1(), NULL);
		HMAC_Update(&ctx, reinterpret_cast<const unsigned char*>(word.c_str()), word.length());
		HMAC_Final(&ctx, result, &result_len);	
#if DEBUG
		for (size_t i = 0; i < result_len; i++) {
			printf("%02x",result[i]);
		}
		printf("\t");
#endif
		mpz_from_bytes(cache, result, sizeof(result));
		R.push_back(cache);
		this->C.push_back(0); // initialize array with zeros
#if DEBUG
		cout << R[R.size()-1] << endl;
#endif
	HMAC_CTX_cleanup(&ctx);
	
	// Convert polynomial from factored form to standard form
	this->C[0] = 1;
	mpz_class t;
	size_t N = R.size();
	for(size_t j = 0; j < N ; j++){
		for(size_t i = 0; i <= j; i++){
			t = this->C[(N - i + 1) % N];
			MULMOD(this->C[i], -R[j], this->C[i], algPRF::p);
			this->C[i] += t;
		}
	}
}

mpz_class wordSearch::problemGen(mpz_class w)
{
	// Output (σw, τw) = (χ = φκ(w), w)
	return w;
}

int wordSearch::compute(const mpz_class& x, mpz_class& y, mpz_class& t)
{
	// set y to be the direct computation of the polynomial;
	// set t to have the result stored in the exponent along
	// with R(x)
	mpz_class xp = 1; // store powers of x.
	mpz_class i1,i2; // temp registers.
	y = 0;
	t = 1;
	for (size_t i = 0; i < C.size(); i++) {
		// y += c_ix^i
		MULMOD(i1,C[i],xp,algPRF::p);
		y += i1;
		MOD(y,y,algPRF::p);
		
		// t *= t_i^{x^i}
		POWERMOD(i1,T[i],xp,algPRF::q);
		t *= i1;
		MOD(t,t,algPRF::q);
		
		MULMOD(xp,xp,x,algPRF::p);
	}
	return 0;
}

bool wordSearch::verify(const mpz_class& x, mpz_class& y, mpz_class& t)
{
	// remember: degree is size of the vector - 1
	mpz_class z = this->F.CFE(x,this->C.size()-1);
	mpz_class i1,i2,e;
	MULMOD(e,this->a,y,algPRF::p);
	POWERMOD(i1,algPRF::g,e,algPRF::q);
	MULMOD(i2,z,i1,algPRF::q);
	/* DEBUG */
	if (t != i2)
	{
		gmp_printf("t == %Zd\n", t.get_mpz_t());
		gmp_printf("i2 == %Zd\n", i2.get_mpz_t());
	}
	/* DEBUG END */
	return (t == i2);
}

bool wordSearch::searchString(const char* c, mpz_class& y, mpz_class& t)
/* TODO: do we need two versions?  String class has a constructor
 * that takes char*, so unless you want to do this by reference (which
 * may be a good idea) then I think just wrting the string version
 * will suffice.  Or is there some issue that arises from const?  */
{
	assert(strlen(c) > 0);
	return searchString(string(c,sizeof c),y,t);
}
bool wordSearch::searchString(const string w, mpz_class& y, mpz_class& t)
{
	assert(w.length() > 0);
	unsigned int result_len = P_BYTES; // Should be less than size of p 
	unsigned char result[P_BYTES];
	HMAC_CTX ctx;
	ENGINE_load_builtin_engines();
	ENGINE_register_all_complete();
	HMAC_CTX_init(&ctx);
	HMAC_Init_ex(&ctx, this->hmac_key, strlen((char*)this->hmac_key), EVP_sha1(), NULL);
	HMAC_Update(&ctx, reinterpret_cast<const unsigned char*>(w.c_str()), w.length());
	HMAC_Final(&ctx, result, &result_len);
	HMAC_CTX_cleanup(&ctx);
	mpz_class x;
	mpz_from_bytes(x, result, sizeof(result));
	return verify(x,y,t);
}

// TO DO:
// Use strtok to clean up strings
// Make sure words are hashed properly into wordSet to avoid duplicate words (i.e. duplicate coefficients)
// Itterate through wordSet to create HMAC roots
// Test conversion of roots into coefficients.
