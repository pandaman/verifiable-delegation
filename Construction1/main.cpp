/* Test file of the implementation of Construction 1 */

#include <stdlib.h>
#include <tr1/cstdint>
#include <iostream>
#include <fstream>
#include <string>
using std::ifstream;
using std::fstream;
using std::string;
using std::cout;
using std::cin;
using std::endl;
#include <cassert>

#include "gmp-utils.h"
#include "alg-prf.h"
#include "poly-del.h"
#include "word-search.h"

#define DEBUG 1

void testClientServer()
{
	char client_file_name[] = "client/sample.txt";
	polyDel client(client_file_name);
	client.keyGen();
	
	polyDel server;
	client.split(server);
	mpz_class y,t;
	mpz_class x = R.get_z_range(algPRF::p);
	server.compute(x,y,t);
#if DEBUG
	gmp_printf("\nC(%Zd) =\t %Zd\n",x.get_mpz_t(),y.get_mpz_t());
	cout << "x = " << x << endl;
	cout << "Test Passed: " << client.verify(x,y,t) << endl;
#endif	
}

/* to add test functions, just toss them in this array, and
 * then call main with its index as a parameter. */
typedef void (*mainfn)(void);
mainfn mainFunctions[] = {
	&testClientServer // 0
};

int main(int argc, const char *argv[])
{

#if GMP_DEBUG_INFO
	cout << "GMP info:\n"
	<< "cflags:\t\t" << __GMP_CFLAGS << endl
	<< "compiler:\t\t" << __GMP_CC << endl
	<< "bits/limb\t\t" << mp_bits_per_limb << endl
	<< "gmp ver.\t\t" << gmp_version << "\n\n";
#endif

	const char checkFailMsg[][9] = {"fail x_x","check"};
#if DEBUG
	cout << "* * * * * * * Group  Parameters * * * * * * *\n";
	cout << "q\t" << algPRF::q << endl;
	cout << "g\t" << algPRF::g << endl;
	cout << "p\t" << algPRF::p << endl;
	cout << "q is prime: " << checkFailMsg[ISPRIME(algPRF::q)] << endl;
	cout << "p is prime: " << checkFailMsg[ISPRIME(algPRF::p)] << endl;
	cout << "* * * * * * * * * * * * * * * * * * * * * * *\n";
#endif

	mainFunctions[(argc>1)?atol(argv[1]):0]();
	return 0;
}	
