Overview
========

Code for a simple client / server that run the keyword search protocol.


Building
========

On a linux box, run `make`.  However, you need to be sure that you have first
gone to ../primitives/ and ran `make lib`.  You might also want to do the same
in the ../gmp-utils/ directory -- I'm not sure when that was last updated.


Testing
=======

For now, there is only one "user account" that the server is managing.

To run the code, invent a text file that you want to remotely store.  For
testing, this should probably be kind of small.  Call this file "testfile".
Then to delegate, run

   $ ./server

and from another terminal, run

   $ ./client --init testfile

This will construct a polynomial for keyword search, and delegate it to the
server.  The server's (large) copy of the data is in the file "stashedserver"
by default, and the client's (small) bit of data needed for verification is in
"stashedclient" by default.  The client and server will disconnect and
terminate automatically.

Next you will want to test the keyword search.  Run the server again (no
arguments are needed for a local test) and then run the client with

   $ ./client --seach something

in order to search for "something".  A message will appear on the screen
indicating whether or not the search was successful, and whether or not the
server's answer was verified.

BTW, the client and server support a --help option which lists the other
options.


TODO's
======

* Make the server run continuously and create new threads for each client
  request.
* Let the server manage multiple client accounts (you'll have to add something
  to the initial messages, e.g. the client id!).
* Test.

And some less important items...

* It might also be nice to scan all text files in a directory, instead of just
  a single file.
* Command line interface could be cleaned up a little (some options aren't
  really used, for example).
* Tokenization could probably use some work; also the search is case-sensitive
  right now.
* Use HMAC instead of just SHA1?

# vim:ft=rst
