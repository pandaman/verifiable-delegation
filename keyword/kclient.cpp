#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <openssl/sha.h>
#include <unordered_set>
#include <string>
#include <vector>
using std::string;
using std::unordered_set;
using std::vector;

#include <getopt.h>
#include <stdarg.h>

#include "gmp-ntl-conv.h"
#include "poly-del.hpp"
#include "alg-prf.hpp"
#include "defs.h"

#ifdef USE_EC
#include "somefile.h" // your elliptic curve header.
typedef something G; // your elliptic curve type goes here...
#else
#include "multgroup.h"
typedef multGrp G;
#endif

int machineIndex = 0;

/* High-level idea: use the polyDel class for the
 * unerlying functionality, and this class to manage
 * the network transfer, wrapper, etc. */

/* network stuff... */

int sockfd;

void error(const char *msg)
{
	perror(msg);
	exit(0);
}

int initNetwork(char* hostname, int port)
{
	struct sockaddr_in serv_addr;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	struct hostent *server;
	if (sockfd < 0)
		error("ERROR opening socket");
	server = gethostbyname(hostname);
	if (server == NULL) {
		fprintf(stderr,"ERROR, no such host\n");
		exit(0);
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);
	serv_addr.sin_port = htons(port);
	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
		error("ERROR connecting");
	/* connection now ready to send / recv. */
	return 0;
}

int shutdownNetwork()
{
	shutdown(sockfd,2);
	unsigned char dummy[64];
	ssize_t r;
	do {
		r = recv(sockfd,dummy,64,0);
	} while (r != 0 && r != -1);
	close(sockfd);
	return 0;
}

/* end network stuff. */

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Run keyword search protocol (client).\n\n"
"   --port       PORT      connect on PORT. defaults to %lu\n"
"   --input      FILE      read inputs from FILE. defaults to %s\n"
"   --output     FILE      send output to FILE (default is stdout)\n"
"   --host       HOST      connect to HOST. default is %s\n"
"   --init       FILE      upload file FILE\n"
"   --search     WORD      searches server for WORD\n"
"   --help                 show this message and exit.\n";

/* setup the delegation so that we can search for keywords
 * in a file.  NOTE: this might be more interesting if we
 * took a directory tree instead of a single file. */
/* construct / delegate the polynomial $prod_{w\in S} (x-H(w))$, */
int setup(char* fname)
{
	typedef unordered_set<string> HS;
	HS S;
	FILE* f;
	f = fopen(fname,"rb");
	char* line = 0;
	char* token;
	size_t len;
	ssize_t read;
	char delims[] = " ,.:;_\n\r\t*-=()";
	unsigned char output[20]; // for hash output.
	while ((read = getline(&line,&len,f)) != -1) {
		token = strtok(line,delims);
		while(token) {
			S.insert(token);
			token = strtok(NULL,delims);
		}
	}
	free(line);
	fclose(f);

	/* now construct the coefficients of a massive polynomial
	 * based on the words of the file we just read. */
	vec_ZZ_p roots;
	ZZ tmp;
	ZZ_p hashv; // store hash as an integer.
	HS::iterator it;
	for (it=S.begin(); it!= S.end(); it++) {
		SHA1((const unsigned char*)it->c_str(),it->length(),output);
		/* now convert to mpz_t, and multiply into the accumulator. */
		ZZFromBytes(tmp,output,20);
		conv(hashv,tmp);
		append(roots,hashv);
	}
	/* now build a polynomial from the roots. */
	ZZ_pX P;
	BuildFromRoots(P,roots);
	polyDel<G> D(P);
	D.keyGen();
	/* first have to let the server know what we want to do: */
	int curOp = upload;
	int n = write(sockfd,&curOp,sizeof(int));
	if (n < 0) error("ERROR writing operation to socket\n");
	/* save the public part in pPub, and clear out the arrays
	 * from our own copy: */
	polyDel<G> pPub;
	D.split(pPub);
	pPub.stash(sockfd); // write the public part over the network
	/* now write our (small) verification info locally: */
	D.stash("stashedclient");
	return 0;
}

int search(char* kw)
{
	/* tell server we want to search: */
	int curOp = query;
	int n = write(sockfd,&curOp,sizeof(int));
	if (n < 0) error("ERROR writing operation to socket\n");

	unsigned char output[20];
	SHA1((unsigned char*)kw,strnlen(kw,KEYWORD_MAX_LEN),output);
	ZZ tmp;
	ZZ_p hashv;
	ZZFromBytes(tmp,output,20);
	conv(hashv,tmp);
	zz_to_file(sockfd,hashv);
	/* server should respond with two integers and a group element: */
	ZZ_p x,y;
	G t;
	zz_from_file(sockfd,x);
	zz_from_file(sockfd,y);
	t.from_file(sockfd);

	/* verify server's answer: */
	polyDel<G> D;
	D.unstash("stashedclient");
	bool v = D.verify(x,y,t);

	if (y == 0)
		fprintf(stderr, "server says '%s' was found.\n",kw);
	else
		fprintf(stderr, "server says '%s' was *NOT* found.\n",kw);
	if (v) fprintf(stderr, "server's answer was verified.\n");
	else fprintf(stderr, "server's answer was *NOT* verified.\n");
	return 0;
}

int main(int argc, char *argv[])
{
	static int help=0;
	static struct option long_opts[] = {
		{"port",       required_argument, 0, 'p'},
		{"input",      required_argument, 0, 'i'},
		{"output",     required_argument, 0, 'o'},
		{"host",       required_argument, 0, 'h'},
		{"init",       required_argument, 0, 'I'},
		{"search",     required_argument, 0, 's'},
		{"help",       no_argument,       &help,    1},
		{0,0,0,0} // this denotes the end of our options.
	};
	int c; // holds an option
	int opt_index = 0;
	int port = 31337;
	char fnameIn[FNAME_MAX_LEN] = "input ";
	fnameIn[5] = '0' + machineIndex;
	char fnameOut[FNAME_MAX_LEN] = ""; // defaults to stdout.
	char hostname[FNAME_MAX_LEN] = "localhost";
	char word[KEYWORD_MAX_LEN] = "";
	/* ensure these are c-strings.  set null char, and don't
	 * copy more than len-1 chars */
	fnameIn[FNAME_MAX_LEN-1] = 0;
	fnameOut[FNAME_MAX_LEN-1] = 0;
	hostname[FNAME_MAX_LEN-1] = 0;
	word[KEYWORD_MAX_LEN-1] = 0;
	bool doSearch = false;
	bool doInit = false;
	while ((c = getopt_long(argc, argv, "p:i:o:h:I:s:",
					long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'p':
				port = atoi(optarg);
				break;
			case 'i':
				strncpy(fnameIn,optarg,FNAME_MAX_LEN-1);
				break;
			case 'o':
				strncpy(fnameOut,optarg,FNAME_MAX_LEN-1);
				break;
			case 'h':
				strncpy(hostname,optarg,FNAME_MAX_LEN-1);
				break;
			case 'I':
				strncpy(fnameIn,optarg,FNAME_MAX_LEN-1);
				doInit = true;
				break;
			case 's':
				strncpy(word,optarg,KEYWORD_MAX_LEN-1);
				doSearch = true;
				break;
			case '?':
				return 1;
		}
	}

	if (help) {
		printf(usage,argv[0], port, fnameIn, hostname);
		return 0;
	}

	int rcode = 0;

	/* make sure we initialize the PRF: */
	algPRF<G>::init();

	/* at this point, we will always be using the network, so get it started: */
	initNetwork(hostname,port); // if this fails, then it also exits...

	if (doInit) {
		rcode = setup(fnameIn);
	}
	else if (doSearch) {
		rcode = search(word);
	}

	shutdownNetwork();

	return rcode;
}

