/* a few constants and such, used by both client and server. */
#pragma once

#define FNAME_MAX_LEN 128
#define KEYWORD_MAX_LEN 128
#define Z_MAX_BYTES 512

enum operation {
	upload, // initial setup; upload the polynomial
	query   // query for a keyword
};

