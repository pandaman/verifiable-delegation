// Higher level idea:
// The server has three important parts: first, it must 
// accommodate a continuous connection from some client; 
// second it must accommodate simultaneous connections from
// multiple clients; and third, it must service the client
// requests. These implementations are detailed below:

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include <unordered_set>
#include <string>
#include <vector>
#include <deque>
using std::string;
using std::unordered_set;
using std::vector;
using std::deque;

#include <getopt.h>
#include <stdarg.h>

#include "gmp-utils.h"
#include "poly-del.h"
#include "alg-prf.h"
#include "defs.h"

int machineIndex = 1;
int listensock, sockfd, pid;

/* High-level idea: use the polyDel class for the
 * unerlying functionality, and this class to manage
 * the network transfer, wrapper, etc. */

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

int setup()
{
	polyDel myCopy;
	myCopy.unstash(sockfd); // read from network (???)
	/* And now write it to a file.  TODO: in the future, this file should
	 * be specific to the client; for now, we just assume there's only one
	 * client. */
	myCopy.stash("stashedserver");
	return 0;
}

/* respond to client's query */
int search()
{
	mpz_class x,y,t;
	/* first read the hash of the keyword from the network. */
	mpz_from_file(sockfd,x);
	/* run compute() for the polyDel */
	polyDel D;
	D.unstash("stashedserver");
	D.compute(x,y,t);
	/* send them back: */
	mpz_to_file(sockfd,x);
	mpz_to_file(sockfd,y);
	mpz_to_file(sockfd,t);
	return 0;
}

/* network stuff... */

int networkActions()
{
	int curOp;
	int n = read(sockfd, &curOp, sizeof(int)); // ssize_t might be better than int.
	if (n < 0)
		error("ERROR reading from socket\n");
	switch (curOp)
	{
		case upload:
			setup();
			break;
		case query:
			search();
			break;
		default:
			fprintf(stderr, "Unknown operation %d\n", curOp);
			return -1;
	}
	unsigned char dummy[64];
	ssize_t r;
	do
	{
		r = recv(sockfd, dummy, 64, 0);
	}while (r != 0 && r != -1);
	close(sockfd);
}

void networkEngine()
{
	// We'll allow a max of 1000 consecutive connections.
	// Not that this has any importance, but maybe establishing a limit
	// might be a good idea...for now.
	socklen_t clilen;
	struct sockaddr_in  cli_addr;
	int limit = 0;
	while (limit != 1000)
	{
		sockfd = accept(listensock, (struct sockaddr *) &cli_addr, &clilen);
		if (sockfd < 0)
			error("ERROR on accept\n");
		pid = fork();
		if (pid < 0)
			error("ERROR on fork\n");
		if (pid == 0)
		{
			close(listensock);
			networkActions();
			exit(0);
		}
		else close (sockfd);
		limit++;
	}
}

int initNetwork(char* hostname, int port)
{
	int reuse = 1;
	struct sockaddr_in serv_addr;
	listensock = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(listensock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
	// TODO: I think if you make sure the client closes first,
	// this won't be an issue.
	if (listensock < 0)
		error("ERROR opening socket");
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	if (bind(listensock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		error("ERROR on binding");
	listen(listensock,5);
	networkEngine();
	// sockfd = accept(listensock, (struct sockaddr *) &cli_addr, &clilen);
	// if (sockfd < 0)
	// 	error("ERROR on accept");
	// // the connection is now established, and ready to send / recv data. 
	// // read a single integer to determine which operation we are supposed
	// // to perform:
	// int curOp;
	// int n = read(sockfd,&curOp,sizeof(int));
	// if (n < 0) error("ERROR reading from socket\n");
	// // else, we have the operation. for now, we'll directly
	// // call a handler for it from here; but in the future...:
	// // TODO:
	// // don't bail out on error, just return -1
	// // on success, start a new thread with the desired function,
	// // and with the file descriptor of the new socket; then we'll
	// // keep on listening for other connections. 
	// switch (curOp) {
	// 	case upload:
	// 		setup();
	// 		break;
	// 	case query:
	// 		search();
	// 		break;
	// 	default:
	// 		fprintf(stderr, "Unknown operation: %d\n",curOp);
	// 		return -1;
	// }
	return 0;
}

int shutdownNetwork()
{
	unsigned char dummy[64];
	ssize_t r;
	do {
		r = recv(sockfd,dummy,64,0);
	} while (r != 0 && r != -1);
	close(sockfd);
	/* TODO: keep listening for more connections? */
	close(listensock);
	return 0;
}

/* end network stuff. */

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Run keyword search protocol (server).\n\n"
"   --port       PORT      listen on PORT. defaults to %lu\n"
"   --input      FILE      read inputs from FILE. defaults to %s\n"
"   --output     FILE      send output to FILE (default is stdout)\n"
"   --host       HOST      connect to HOST. default is %s\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[])
{
	static int help=0;
	static struct option long_opts[] = {
		{"port",       required_argument, 0, 'p'},
		{"input",      required_argument, 0, 'i'},
		{"output",     required_argument, 0, 'o'},
		{"host",       required_argument, 0, 'h'},
		{"help",       no_argument,       &help,    1},
		{0,0,0,0} // this denotes the end of our options.
	};
	int c; // holds an option
	int opt_index = 0;
	int port = 31337;
	char fnameIn[FNAME_MAX_LEN] = "input ";
	fnameIn[5] = '0' + machineIndex;
	char fnameOut[FNAME_MAX_LEN] = ""; // defaults to stdout.
	char hostname[FNAME_MAX_LEN] = "localhost";
	/* ensure these are c-strings.  set null char, and don't
	 * copy more than len-1 chars */
	fnameIn[FNAME_MAX_LEN-1] = 0;
	fnameOut[FNAME_MAX_LEN-1] = 0;
	hostname[FNAME_MAX_LEN-1] = 0;
	while ((c = getopt_long(argc, argv, "p:i:o:h:",
					long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'p':
				port = atoi(optarg);
				break;
			case 'i':
				strncpy(fnameIn,optarg,FNAME_MAX_LEN-1);
				break;
			case 'o':
				strncpy(fnameOut,optarg,FNAME_MAX_LEN-1);
				break;
			case 'h':
				strncpy(hostname,optarg,FNAME_MAX_LEN-1);
				break;
			case '?':
				return 1;
		}
	}

	if (help) {
		printf(usage,argv[0], port, fnameIn, hostname);
		return 0;
	}

	int rcode = 0;

	initNetwork(hostname,port);
	 // as it is, initNetwork actually handles the protocol as well.
	 // later on, it will create new threads to handle the different
	 // requests, and run that loop forever. Either way, there's not
	 // much for us to do here.
	//shutdownNetwork();
	close(listensock);
	close(sockfd);

	return rcode;
}
