/* implementation of multiplicative group, based on GMP */
#include "multgroup.h"
#include "gmp-utils.h"
#include "gmp-ntl-conv.h"

uint32_t gBytes[] = {
	#include "g-1024-160.txt"
};
uint32_t qBytes[] = {
	#include "q-1024-160.txt"
};
uint32_t pBytes[] = {
	#include "p-1024-160.txt"
};

const mpz_class multGrp::q = mpz_from_dwords(qBytes,sizeof(qBytes)/sizeof(qBytes[0]));
const mpz_class multGrp::g = mpz_from_dwords(gBytes,sizeof(gBytes)/sizeof(gBytes[0]));
const mpz_class multGrp::p = mpz_from_dwords(pBytes,sizeof(pBytes)/sizeof(pBytes[0]));
unsigned short mBytes = sizeof(pBytes)-1;

/** group element constructors should set elements to the identity. */
multGrp::multGrp()
{
	this->elt = 1;
}

multGrp::multGrp(const multGrp& b)
{
	this->elt = b.elt;
}

multGrp::~multGrp()
{
}

int multGrp::mul(multGrp& r, const multGrp& a, const multGrp& b)
{
	MULMOD(r.elt,a.elt,b.elt,q);
	return 0;
}

int multGrp::exp(multGrp& r, const multGrp& a, const ZZ& b)
{
	mpz_class bz;
	zztompz(bz,b);
	/* and now we can exponentiate as usual. */
	/* TODO: if a is the generator, use the better fixed base method? */
	POWERMOD(r.elt,a.elt,bz,q);
	return 0;
}

int multGrp::exp(multGrp& r, const multGrp& a, const ZZ_p& b)
{
	return multGrp::exp(r,a,rep(b));
}

ZZ multGrp::order()
{
	ZZ ord;
	mpztozz(ord,p);
	return ord;
}

int multGrp::getGenerator(multGrp& g)
{
	g.elt = multGrp::g;
	return 0;
}

void multGrp::operator*=(const multGrp& b)
{
	MULMOD(this->elt,this->elt,b.elt,q);
	return;
}

void multGrp::operator=(const multGrp& b)
{
	this->elt = b.elt;
	return;
}

bool multGrp::operator==(const multGrp& b)
{
	/* use the overloaded operator from gmp C++ interface: */
	return (this->elt == b.elt);
}

bool multGrp::equals(const multGrp& b)
{
	return (this->elt == b.elt);
}

void multGrp::setToIdent()
{
	this->elt = 1;
}

void multGrp::clear()
{
	/* TODO: find a good way to zero mpz variables. */
	return;
}

void multGrp::to_file(int fd)
{
	// since the parameters q,g,p are hard-coded,
	// we just need to save this->elt
	mpz_to_file(fd,this->elt);
}

void multGrp::from_file(int fd)
{
	// just need to read this->elt
	mpz_from_file(fd,this->elt);
}
