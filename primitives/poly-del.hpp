/* Interface for polynomial delegation protocol. */

#include "multgroup.h"
#include "alg-prf.hpp"
#include "gmp-ntl-conv.h"
#include <fstream>
#include <cassert>
#include <unistd.h>
#include <vector>
using std::vector;
#include <NTL/ZZ_pX.h>
using namespace NTL;

#define Z_MAX_BYTES 512
/** this determines whether or not to turn on the non-black box
 * optimization when generating tags from the algebraic PRF.
 * The optimization works for our only implementation, so currently,
 * there is no reason not to turn it on. */
#define NON_BLACK_BOX_PRF 1

/** this class will encapsulate the functionality of the client
 * and the server for the basic polynomial delegation scheme of
 * [BGV2011](http://eprint.iacr.org/2011/132)
 *
 * NOTE: The only difference in an actual protocol execution is what data is
 * present in the two instances (client and server). */

template <typename G>
class polyDel
{
public:
	polyDel() {}
	polyDel(const ZZ_pX& poly) : P(poly)
	{
		this->savedDegree = deg(poly);
	}
	/* TODO: do you use this? */
// 	polyDel(const vector<ZZ_p>& Coeffs)
// 	{
// 		// NOTE if you need this, you can (and should) call the
// 		// normalize() function on the polynomial after you've
// 		// constructed it from the vector.
// 	}
	~polyDel() {}
	/* ATM, no dynamic memory allocated by us, and
	 * no need for our own assignment operator. */
	//polyDel& operator=(const polyDel& RHS);

	int keyGen()
	{
		/* check: do we have a polynomial to delegate? */
		if (IsZero(this->P)) return -1;
		/* first run keygen for the algebraic PRF */
		this->F.keyGen();
		random(this->a);
		/* now generate the elements g^{ac_i}*F_K(i), and store in T */
		G t;
		/* NOTE: small typo in paper.  The coeffs r_i are of course
		 * not the output of the PRF. */
		this->T.clear();
#if NON_BLACK_BOX_PRF
		ZZ_p x,e;
		ZZ iz;
#endif
		for (long i = 0; i < this->P.rep.length(); i++) {
			// if we open up the black box a little, we can do this
			// more efficiently.
#if NON_BLACK_BOX_PRF
			/* compute the entire exponent c_ia + k_0k_1^i first:  */
			iz = i;
			x = power(this->F.k1,iz);
			e = a*this->P.rep[i] + this->F.k0 * x;
			G::exp(t,this->F.g,e);
#else
			G::exp(t,this->F.g,a*this->P.rep[i]);
			t *= this->F(i);
#endif
			this->T.push_back(t);
		}
		return 0;
	}

	int compute(const ZZ_p& x, ZZ_p& y, G& t)
	{
		// set y to be the direct computation of the polynomial:
		eval(y,this->P,x);
		/* NOTE: would it be faster to do the plain evaluation
		 * ourselves below, since we are already computing all
		 * the powers of x? */
		// set t to have the result stored in the exponent along with R(x)
		ZZ_p xp;
		xp = 1; // store powers of x.
		G tmp;
		t.setToIdent();
		for (long i = 0; i < this->P.rep.length(); i++) {
			// t *= t_i^{x^i}
			G::exp(tmp,T[i],xp);
			t *= tmp;
			xp *= x;
		}
		return 0;
	}
	/* TODO: I think we should remove x from the verify parameters,
	 * since a fresh random one should be used every time this
	 * function is called. */
	bool verify(const ZZ_p& x, ZZ_p& y, G& t)
	{
		G z = this->F.CFE(x,this->savedDegree);
		G tmp,tmp2;
		G::exp(tmp,this->F.g,this->a*y);
		G::mul(tmp2,z,tmp);
		//return (t == tmp2);
		// for clarity, use the equals function:
		return t.equals(tmp2);
	}

	/** the following splits the structure into a large public part
	 * (the pub parameter) and removes the bulky arrays, leaving
	 * only the private key for the PRF in *this. */
	void split(polyDel& pub)
	{
		pub.P = this->P;
		pub.T = this->T;
		pub.F.clear();
		pub.a = 0;

		/* now clear the unneeded parts of *this, and make sure
		 * we save the degree, in case we don't have it: */
		this->savedDegree = deg(this->P);
		this->P.kill();
		this->T.clear();
	}
	/** writes structure (binary format) to a file: */
	int stash(const char* fname)
	{
		FILE* f = fopen(fname,"wb");
		if (!f) {
			fprintf(stderr, "Couldn't open file %s\n",fname);
			return -1;
		}
		this->stash(fileno(f));
		fclose(f);
		return 0;
	}

	/// writes to an open file descriptor
	int stash(int fd)
	{
		//int n; // store return of reads  / writes
		size_t i,oCount;
		zz_to_file(fd,this->F.k0);
		zz_to_file(fd,this->F.k1);
		zz_to_file(fd,this->a);
		oCount = this->P.rep.length();
		write(fd,&oCount,sizeof(size_t)); // how many coeffs
		// now write the integers.
		for (i = 0; i < oCount; i++) {
			zz_to_file(fd,this->P.rep[i]);
		}
		oCount = this->T.size();
		write(fd,&oCount,sizeof(size_t)); // how many integers in T
		for (i = 0; i < oCount; i++) {
			this->T[i].to_file(fd);
		}
		if (deg(this->P) != -1) this->savedDegree = deg(this->P);
		write(fd,&this->savedDegree,sizeof(long));
		return 0;
	}

	/// reads previously stash()'d file
	int unstash(const char* fname)
	{
		FILE* f = fopen(fname,"rb");
		if (!f) {
			fprintf(stderr, "Couldn't open file %s\n",fname);
			return -1;
		}
		this->unstash(fileno(f));
		fclose(f);
		return 0;
	}
	/// reads from an open file descriptor
	int unstash(int fd)
	{
		size_t i,iCount;
		zz_from_file(fd,this->F.k0);
		zz_from_file(fd,this->F.k1);
		/* NOTE: keygen would just select the above values randomly,
		 * so I don't think we are breaking any class invariants
		 * here by setting them directly. */
		zz_from_file(fd,this->a);
		read(fd,&iCount,sizeof(size_t));
		this->P.SetMaxLength(iCount);
		ZZ_p tmp;
		for (i = 0; i < iCount; i++) {
			zz_from_file(fd,tmp);
			SetCoeff(this->P,i,tmp);
		}
		read(fd,&iCount,sizeof(size_t));
		this->T.resize(iCount);
		for (i = 0; i < iCount; i++) {
			this->T[i].from_file(fd);
		}
		read(fd,&this->savedDegree,sizeof(long));
		return 0;
	}

	// FIXME: after testing, put this back.
//private:

	/* "public" data (known to client and server) */
	ZZ_pX P; ///< the polynomial
	/** A subtle point -- once you have called split(), the polynomial
	 * will be destroyed, and set to 0.  So, if you ever need the
	 * degree beyond that point, you will have to have it saved
	 * somewhere.  savedDegree is that somewhere. */
	long savedDegree;
	vector<G> T; ///< group elements hiding the coeff's of R.

	/* "private" data (only known to the client) */
	algPRF<G> F; ///< underlying algebraic PRF
	ZZ_p a; ///< secret value used to construct tags in T
};
