#include "multgroup.h"
#include <cstdio>
#include <iostream>
using std::cout;
using std::endl;

#include "alg-prf.hpp"
#include "poly-del.hpp"

int testPolyDel()
{
	// invent a polynomial to outsource:
	ZZ_pX f;
	f.SetMaxLength(32);
	cout << "coeffs = { ";
	/* NOTE: I think you need to allocate enough space for f
	 * in general.  By default, NTL will not check the length
	 * inside of operator[] */
	for (size_t i = 1; i < 10; i++) {
		SetCoeff(f,i-1,i*i*i);
		cout << f.rep[i-1] << " ";
	}
	cout << "}" << endl;

	polyDel<multGrp> D(f);
	D.keyGen();
	// invent a place to evaluate the polynomial:  <-- x in [0,d]?
	ZZ_p x;
	x = 2202394270;
	polyDel<multGrp> server;
	D.split(server);
	ZZ_p y;
	multGrp t;
	server.compute(x,y,t);
	cout << "\nf(" << x << ") = " << y << endl;
	cout << "Verification: " << D.verify(x,y,t) << endl;
	return 0;
}

int testPRF()
{
	algPRF<multGrp> F;
	F.keyGen();
	for (size_t i = 0; i < 5; i++) {
		gmp_printf("F(%lu) = %Zd\n",i,F(i).elt.get_mpz_t());
	}

	ZZ_p x;
	unsigned long d = 10;
	for (size_t i = 1718233; i < 1718235; i++) {
		x = i;
		gmp_printf("   CFE(%lu,%lu) = %Zd\n",i,d,F.CFE(x,d).elt.get_mpz_t());
		gmp_printf("nonCFE(%lu,%lu) = %Zd\n",i,d,F.nonCFE(x,d).elt.get_mpz_t());
	}
	return 0;
}

int testStash()
{
	// generate a polyDel object, write to a file,
	// read from that file, and see if anything changed.
	ZZ_pX f;
	f.SetMaxLength(32);
	cout << "coeffs = { ";
	for (size_t i = 1; i < 10; i++) {
		SetCoeff(f,i-1,i*i*i);
		cout << f.rep[i-1] << " ";
	}
	cout << "}" << endl;

	polyDel<multGrp> D(f);
	D.keyGen();

	// now the parameters are set; write it.
	cout << "D.F.k0 = " << D.F.k0 << endl;
	cout << "D.F.k1 = " << D.F.k1 << endl;
	D.stash("teststash");
	polyDel<multGrp> E;
	E.unstash("teststash");
	cout << "E.F.k0 = " << E.F.k0 << endl;
	cout << "E.F.k1 = " << E.F.k1 << endl;
	// well, it reads one value properly.  check the rest without printing.
	/* first check the sizes */
	if (deg(D.P) != deg(E.P))
		fprintf(stderr, "Degrees are %li,%li respectively.\n",
				deg(D.P),deg(E.P));
	if (D.T.size() != E.T.size())
		fprintf(stderr, "Tag sizes are %lu,%lu respectively.\n",
				D.T.size(),E.T.size());
	for (long i = 0; i < D.P.rep.length(); i++) {
		if (D.P.rep[i] != E.P.rep[i])
			fprintf(stderr, "coeffs differ at %li\n",i);
	}
	for (size_t i = 0; i < D.T.size(); i++) {
		if (! D.T[i].equals(E.T[i]))
			fprintf(stderr, "coeffs differ at %lu\n",i);
	}
	return 0;
}

int main(void)
{
	/* NOTE: you must call setModulus() before declaring any
	 * algPRF variables.  This is an artifact of the way NTL
	 * deals with the ZZ_p class. */
	algPRF<multGrp>::init();
	//return testPRF();
	return testStash();
	return testPolyDel();
}
