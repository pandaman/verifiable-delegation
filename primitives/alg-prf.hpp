/* interface for algebraic PRF with small domain */

#pragma once

#include <NTL/ZZ_p.h>
using namespace NTL;
#if LINUX
#include <cstdint>
#else
#include <tr1/cstdint>
#endif

/** algebraic PRF construction from [BGV2011](http://eprint.iacr.org/2011/132)
 * */
template <typename G>
class algPRF
{
public:
	/* construction / desctruction */
	algPRF()
	{
		// initialize the generator.
		G::getGenerator(this->g);
	}
	~algPRF(){};


	/** you must call this function before declaring any algPRF variables.
	 * It will set the modulus p to be the size of the underlying group
	 * as well as initialize the pseudorandom generator */
	static int init()
	{
		/* set the modulus p: */
		ZZ_p::init(G::order());
		/* and initialize the PRG: */
		FILE* frand = fopen("/dev/urandom","rb");
		unsigned char seed[32];
		fread(seed,1,32,frand);
		fclose(frand);
		ZZ tmp;
		ZZFromBytes(tmp,seed,32);
		SetSeed(tmp);
		/* TODO: replace with /dev/random for release version */
		return 0;
	}

	/** Key generation.
	 * NOTE: there is no security parameter, since this is determined
	 * by the group you are using.  To increase the security parameter,
	 * instantiate this class with a larger group. */
	int keyGen()
	{
		random(this->k0);
		random(this->k1);
		return 0;
	}

	/** compute F_K(i) for i\in Z_p */
	G operator()(const ZZ_p& i)
	{
		/* Compute e = k0*k1^i mod p: */
		ZZ_p t = power(this->k1,rep(i));
		ZZ_p e = this->k0 * t;
		/* and now invoke the *group's* powering algo: */
		G rv;
		G::exp(rv,g,e);
		return rv;
	}

	/* overload for long values. */
	G operator()(unsigned long i)
	{
		ZZ iz;
		iz = i;
		ZZ_p t = power(this->k1,iz);
		ZZ_p e = this->k0 * t;
		G rv;
		G::exp(rv,g,e);
		return rv;
	}

	/** closed-form evaluation of \prod_{i=0..d} F_K(i)^{x^i} */
	G CFE(const ZZ_p& x, unsigned long d)
	{
		/* need to compute g^e mod q where the exponent is kind of nasty:
		 * e = k0(1-k1^{d+1}x^{d+1}) / (1-k1x)*/
		ZZ_p i1,i2,i3; // registers to work with.
		i1 = k1*x;
		i2 = power(i1,d+1);
		i3 = k0*i2;
		i3 = (k0 - i3);
		// now divide by (1-k1x)
		i2 = (1 - i1); // i2 = (1-k1x)
		i1 = inv(i2);
		i2 = i1*i3;
		// i2 now has the exponent.
		G rv;
		G::exp(rv,g,i2);
		return rv;
	}

	/** for debug / testing, we provide a non-closed-form
	 * method of evaluation. */
	/* The best way to think of it is that F takes values in a finite
	 * abelian group G, which is a Z_p-module.  So general "formulas"
	 * in variables Y_i in G are of the form $c\prod_{i=0}^d c_iY_i$
	 * where the Y_i take values in G, and the c_i are in Z_p.  In our
	 * case, we'll get Y_i = F(i), and c_i = x^i for some x\in Z_p.
	 * */
	/* NOTE: this could be done more efficiently if it ever mattered.
	 * Currently, I don't think we use this anywhere else, except to verify
	 * the results of the closed form evalutation. */
	G nonCFE(const ZZ_p& x, unsigned long d)
	{
		ZZ_p i1;
		G i2,i3,a; // these should be set to the identity (1).
		for (size_t i = 0; i <= d; i++) {
			i1 = power(x,i);
			i2 = (*this)(i);
			G::exp(i3,i2,i1);
			a *= i3; // NOTE: we require *= defined for our group.
		}
		return a;
	}

	/* set the private keys to 0. */
	int clear()
	{
		/* TODO: need to figure out a good way to "securely" erase
		 * ZZ_p instances.  I guess you could go through and
		 * set the bits to all be 0... */
		NTL::clear(this->k0);
		NTL::clear(this->k1);
		return 0;
	}

	/// the private key.
	ZZ_p k0,k1;

	/* TODO: you should make g static. */
	G g; ///< generator of group.
	//static const G g;
};
