/* gmp-utils.cpp
 * Implementation of gmp-utils.h
 * */

#include "gmp-utils.h"
#include <unistd.h>


#define Z_MAX_BYTES 512
unsigned char zBufGlobal[Z_MAX_BYTES]; // for temporary storage.

// TODO: this method of generating random integers is not sufficient
// for crypto stuff.
gmp_randclass R(gmp_randinit_default);

/* NOTE: modular multiplication will require two function calls, I think.
 * for a single operation, the two calls are fine.  If you have occasion to do
 * a bunch in a row, then use montgomery stuff is better.
 * */

/* compute a*b mod n; store result in r */
void MULMOD(mpz_class& r, const mpz_class& a, const mpz_class& b, const mpz_class& n)
{
	MUL(r,a,b);
	MOD(r,r,n);
}

int powm_fixedbase(mpz_ptr r, mpz_srcptr g, mpz_srcptr y,
		mpz_srcptr n, mpz_t** sxtPow)
{
	/* compute max # of base 16 digits: */
	size_t nlimbs = y->_mp_size;
	size_t nibblesPerDigit = sizeof(mp_limb_t) * 2;
	size_t npows = ((nlimbs+1) * nibblesPerDigit);
	size_t i;
	mpz_t* spow = *sxtPow; // convenient alias for 16th powers array
	if (*sxtPow == 0) {
		/* for now, not going to optimize the precomputation part much,
		 * since it is not supposed to happen all that often. */
		*sxtPow = (mpz_t*) malloc(npows * sizeof(mpz_t));
		spow = *sxtPow;
		mpz_init(spow[0]);
		mpz_set(spow[0],g);
		for (i = 1; i < npows; i++) {
			mpz_init(spow[i]);
			mpz_powm_ui(spow[i],spow[i-1],16,n);
		}
	}
	mpz_t b; mpz_init(b);
	mpz_set_ui(b,1);
	mpz_set_ui(r,1);
	mp_limb_t yd;
	mp_limb_t mask16 = 0xF;
	for (size_t j=15; j>0; j--) {
		i=0; // i will now index nibbles
		for (size_t k=0; k<nlimbs; k++) {
			yd = y->_mp_d[k];
			for (size_t t=0; t<nibblesPerDigit; t++) {
				if ((yd & mask16) == j) {
					mpz_mul(b,b,spow[i]);
					mpz_mod(b,b,n);
				}
				yd >>= 4;
				i++;
			}
		}
		mpz_mul(r,r,b);
		mpz_mod(r,r,n);
	}

	return (int)npows;
}
void randomPrime(mpz_class& p, unsigned long nBits)
{
	do {
		p = R.get_z_bits(nBits);
	} while (!ISPRIME(p));
	return;
}

void genSafePrime(mpz_class& p, unsigned long nBits)
{
	mpz_class p1; // store (p-1) / 2
	while (true) {
		randomPrime(p,nBits);
		// NOTE: since p1 is odd, p >> 1 == (p-1) / 2
		p1 = p >> 1;
		if (ISPRIME(p1))
			return;
	}
}

void mpz_from_bytes(mpz_class& r, const unsigned char* inBuf, size_t l)
{
	mpz_import(r.get_mpz_t(),l,-1,1,0,0,inBuf);
}

void bytes_from_mpz(unsigned char*& outBuf, size_t* l, const mpz_class& n)
{
	outBuf = (unsigned char*)mpz_export(outBuf,l,-1,1,0,0,n.get_mpz_t());
}

mpz_class mpz_from_dwords(uint32_t* inBuf, size_t nWords, int endian)
{
	mpz_t tmp;
	mpz_init(tmp);
	mpz_import(tmp,nWords,endian,4,0,0,inBuf);
	mpz_class rv = mpz_class(tmp);
	mpz_clear(tmp);
	return rv;
}

/* use bytes_from_mpz to write n to a file in the following format:
 * 8 bytes for the length of n, followed by the bytes of n, produced
 * by the above function bytes_from_mpz */
void mpz_to_file(int fd, const mpz_class& n)
{
	unsigned char* zBuf = zBufGlobal;
	size_t zLen = 0;
	bytes_from_mpz(zBuf,&zLen,n);
	write(fd,&zLen,sizeof(size_t));
	write(fd,zBuf,zLen);
}

/* reads an integer from an open file descriptor.
 * uses the same format as mpz_to_file */
void mpz_from_file(int fd, mpz_class& n)
{
	unsigned char* zBuf = zBufGlobal;
	size_t zLen = 0;
	read(fd,&zLen,sizeof(size_t));
	read(fd,zBuf,zLen);
	mpz_from_bytes(n,zBuf,zLen);
}
