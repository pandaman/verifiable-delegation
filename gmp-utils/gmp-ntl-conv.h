/* Some conversion routines for GMP <--> NTL,
 * as well as a few other utility functions... */

#pragma once
//#include "gmp-utils.h"
#include <gmpxx.h>
#include <NTL/ZZ_p.h>
using NTL::ZZ;
using NTL::ZZ_p;

/* for convenience, and to save calls to malloc + free,
 * we set a max byte length for our integers. */
//#define Z_MAX_BYTES 256
#define MP_MAX_BYTES 256

void zztompz(mpz_class& r, const ZZ& n);
void mpztozz(ZZ& r, const mpz_class& n);

/* we'll try to use the same format here as we did for gmp:
 * 8 bytes for the length of n, followed by the bytes of n, produced
 * by the above function bytes_from_mpz */
void zz_to_file(int fd, const ZZ& n);
void zz_to_file(int fd, const ZZ_p& n);

/* reads an integer from an open file descriptor.
 * uses the same format as mpz_to_file */
void zz_from_file(int fd, ZZ& n);
void zz_from_file(int fd, ZZ_p& n);
