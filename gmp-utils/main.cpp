#include <stdlib.h>
#if LINUX
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <iostream>
#include <string>
using std::string;
using std::cout;
using std::cin;
using std::endl;
#include "gmp-utils.h"

#include <time.h>

// should give me a microsecond timer.
uint64_t clockGetTime_mu() {
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return (uint64_t)ts.tv_sec * 1000000LL + (uint64_t)ts.tv_nsec / 1000LL;
}

// readtsc function, stolen from wikipedia:
// http://en.wikipedia.org/wiki/Time_Stamp_Counter
extern "C" {
  __inline__ uint64_t rdtsc(void) {
    uint32_t lo, hi;
    __asm__ __volatile__ (      // serialize
    "xorl %%eax,%%eax \n        cpuid"
    ::: "%rax", "%rbx", "%rcx", "%rdx");
    /* We cannot use "=A", since this would use %rax on x86_64 and return only the lower 32bits of the TSC */
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return (uint64_t)hi << 32 | lo;
  }
}

int testMulTiming(unsigned long nTrials, unsigned long nBits, bool useTSC)
{
	/* Here are some basic tests of the C++ interface to gmp.
	 * We'll test how long multiplication takes.  It seems that for a
	 * newer sandybridge, you can multiply 2048 bit integers, and only spend
	 * 2.4 clock cycles per 64 bit limb O_O  (Still slower than the theoretical
	 * throughput, but faster than the latency (3 cc))
	 * */
	mpz_class a,b,c,d;
	//a = R.get_z_bits(nBits);
	b = R.get_z_bits(nBits);
	c = R.get_z_bits(nBits);
	d = R.get_z_bits(nBits);
	uint64_t tStart, tStop;
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	for (unsigned long i = 0; i < nTrials; i++) {
		// POWERMOD(a,b,c,d);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
		MUL(a,b,c);
	}
	if (useTSC) {
		tStop = rdtsc();
		cout << "average TSCs: ";
	} else {
		tStop = clockGetTime_mu();
		cout << "average microseconds: ";
	}
	cout << (double)(tStop - tStart) / (8*nTrials) << endl;
	return 0;
}

int mulmodTest()
{
	/* read 3 integers, a,b,c from standard input, and print the
	 * modular multiplication a*b mod c to stdout.
	 * */
	mpz_class a,b,c,d;
	string s;
	while(true) {
		cin >> s;
		a = s.c_str();
		cin >> s;
		b = s.c_str();
		cin >> s;
		c = s.c_str();
		MULMOD(d,a,b,c);
		cout << d << endl;
	}
	return 0;
}

int powermodTest()
{
	/* read 3 integers, a,b,c from standard input, and print the
	 * modular exponentiation a^b mod c to stdout.
	 * */
	mpz_class a,b,c,d;
	string s;
	while(true) {
		cin >> s;
		a = s.c_str();
		cin >> s;
		b = s.c_str();
		cin >> s;
		c = s.c_str();
		POWERMOD(d,a,b,c);
		cout << d << endl;
	}
	return 0;
}

int primeTest()
{
	/* generating some random prime numbers.  Read the
	 * number of bits from stdin, and then print a
	 * prime of that size to stdout.
	 * */
	uint16_t nBits;
	mpz_class p;
	while (cin >> nBits) {
		randomPrime(p,nBits);
		cout << p << endl;
	}
	return 0;
}

int safePrimeTest()
{
	uint16_t nBits;
	mpz_class p;
	while (cin >> nBits) {
		genSafePrime(p,nBits);
		cout << p << endl;
	}
	return 0;
}

int conversionTest()
{
	unsigned char A[] = {1,2,3,4,5};
	// this should be 1 + 2*256 + 3*256**2 + 4*256**3 + 5*256**4 = 21542142465
	mpz_class n;
	mpz_from_bytes(n,A,5);
	cout << n << endl;
	// and now try this in reverse:
	unsigned char B[5];
	unsigned char* C = B;
	size_t l;
	bytes_from_mpz(C,&l,n);
	for (size_t i = 0; i < 5; i++) {
		cout << (unsigned int)B[i] << "  ";
	}
	// make sure B == C
	cout << endl << reinterpret_cast<unsigned long*>(B) << " "
		<< reinterpret_cast<unsigned long*>(C) << endl;
	// NOTE: the interface for bytes_from_mpz is a little clunky if you have
	// a statically allocated array, but it will work fine otherwise, which
	// will usually(?) be the case.  Here's an example:
	unsigned char* D = 0;
	bytes_from_mpz(D,&l,n);
	for (size_t i = 0; i < 5; i++) {
		cout << (unsigned int)D[i] << "  ";
	}
	cout << endl;
	delete[] D;
	return 0;
}

int powmodFixedTest()
{
	mpz_t r,a,b,n;
	mpz_t* pow16 = 0;
	mpz_init(r); mpz_init(a); mpz_init(b); mpz_init(n);
	gmp_scanf("%Zd %Zd %Zd",a,b,n);
	int npows = powm_fixedbase(r,a,b,n,&pow16);
	/* return value is the size of the allocated array
	 * of 16th powers. */

	for (int i=0; i<npows; i++)
		mpz_clear(pow16[i]);
	free(pow16);
	fprintf(stderr, "allocated %i values.\n",npows);
	gmp_fprintf(stderr, "Answer was: %Zd\n",r);
	mpz_powm(r,a,b,n);
	gmp_fprintf(stderr, "Regular version: %Zd\n",r);
	return 0;
}

int powmodTiming(unsigned long nTrials, unsigned long nBits,
		bool useTSC, bool useFB)
{
	mpz_class r,g,y,n;
	g = R.get_z_bits(nBits);
	y = R.get_z_bits(nBits);
	n = R.get_z_bits(nBits);
	// should make n odd for montgomery.
	mpz_t* table = 0;

	/* don't measure the precomputation: */
	int npows = POWERMOD_FB(r,g,y,n,&table);

	uint64_t tStart, tStop;
	if (useTSC)
		tStart = rdtsc();
	else
		tStart = clockGetTime_mu();

	if (useFB) {
		for (unsigned long i = 0; i < nTrials; i++) {
			POWERMOD_FB(r,g,y,n,&table);
		}
	} else {
		for (unsigned long i = 0; i < nTrials; i++) {
			POWERMOD(r,g,y,n);
		}
	}
	if (useTSC) {
		tStop = rdtsc();
		cout << "average TSCs: ";
	} else {
		tStop = clockGetTime_mu();
		cout << "average microseconds: ";
	}
	cout << (double)(tStop - tStart) / (nTrials) << endl;

	// clear out the table.
	for (int i=0; i<npows; i++)
		mpz_clear(table[i]);
	free(table);
	return 0;
}

int main(int argc, const char *argv[])
{
	/* First, setup our random numbers... */
	// TODO: for an actual application, use /dev/random
	FILE* frand = fopen("/dev/urandom","rb");
	unsigned char seed[32];
	fread(seed,1,32,frand);
	fclose(frand);
	mpz_t S;
	mpz_init(S); // is this necessary?  Not sure.
	mpz_import(S,32,-1,1,0,0,seed);
	mpz_class SC(S);
	R.seed(SC);
	mpz_clear(S);

	bool useTSC = true;
	unsigned long nTrials = 100000;
	unsigned long nBits = 2048;
	cout << "GMP info:\n"
		<< "cflags:\t\t" << __GMP_CFLAGS << endl
		<< "compiler:\t\t" << __GMP_CC << endl
		<< "bits/limb\t\t" << mp_bits_per_limb << endl
		<< "gmp ver.\t\t" << gmp_version << "\n\n";

	int whichTest = 0;
	if (argc > 1)
		whichTest = atoi(argv[1]);

	switch (whichTest) {
		case 0:
			printf("Running prime test...\n");
			return primeTest();
		case 1:
			printf("Running powermod test...\n");
			return powermodTest();
		case 2:
			printf("Running mulmod test...\n");
			return mulmodTest();
		case 3:
			printf("Running multiplication timing test...\n");
			return testMulTiming(nTrials,nBits,useTSC);
		case 4:
			printf("Running safe prime test...\n");
			return safePrimeTest();
		case 5:
			printf("Running conversion test...\n");
			return conversionTest();
		case 6:
			printf("Running powmod fixed-base test...\n");
			return powmodFixedTest();
		case 7:
			printf("Running powmod fixed-base timing test...\n");
			printf("Fixed base results:\n");
			powmodTiming(50,1024,false,true);
			printf("Plain GMP results:\n");
			powmodTiming(50,1024,false,false);
			return 0;
	}
}

