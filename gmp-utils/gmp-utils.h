/* gmp-utils.h
 * This file aims to provide a slightly less cumbersome interface
 * to some common gmp functions, as well as introducing a few utilities
 * that are not found natively in gmp.
 * */

#pragma once

#include "gmpxx.h"

/* for sending over the network, or into buffers, it is convenient
 * to know an upper bound on the size.  We'll set this as 512 bytes. */
#define Z_MAX_BYTES 512

#define POWERMOD(r,x,y,n) \
	mpz_powm(r.get_mpz_t(),x.get_mpz_t(),y.get_mpz_t(),n.get_mpz_t())
#define POWERMOD_UI(r,x,y,n) mpz_powm_ui(r.get_mpz_t(),x.get_mpz_t(),y,n.get_mpz_t())
#define MUL(r,x,y) mpz_mul(r.get_mpz_t(),x.get_mpz_t(),y.get_mpz_t())
#define MOD(r,x,n) mpz_mod(r.get_mpz_t(),x.get_mpz_t(),n.get_mpz_t())
#define ISPRIME(x) mpz_probab_prime_p(x.get_mpz_t(),10)
#define INVERT(r,x,n) mpz_invert(r.get_mpz_t(),x.get_mpz_t(),n.get_mpz_t())
#define CMP(x,y) mpz_cmp(x.get_mpz_t(),y.get_mpz_t())
#define POWERMOD_FB(r,g,y,n,s) \
	powm_fixedbase(r.get_mpz_t(),g.get_mpz_t(),y.get_mpz_t(),n.get_mpz_t(),s)

extern gmp_randclass R;

/* compute a*b mod n; store result in r */
void MULMOD(mpz_class& r, const mpz_class& a, const mpz_class& b, const mpz_class& n);

/* compute g^y mod n, store result in r.  sxtPow should be either a null
 * pointer or a pointer to an array of g,g^16,g^256,... */
int powm_fixedbase(mpz_ptr r, mpz_srcptr g, mpz_srcptr y,
		mpz_srcptr n, mpz_t** sxtPow);

void randomPrime(mpz_class& p, unsigned long nBits);

void genSafePrime(mpz_class& p, unsigned long nBits);

/* convenience functions for reading and writing byte strings */

/* read l bytes of inBuf as an integer (little endian) in base 256.
 * store the result in r. */
void mpz_from_bytes(mpz_class& r, const unsigned char* inBuf, size_t l);

/* write bytes of the integer n into outBuf (little endian).
 * NOTE: if outBuf is null, then this function will allocate the necessary
 * amount of memory, modify outBuf to point to it, and store the amount
 * of storage allocated in *l.  If outBuf is NOT null, then it must have
 * enough storage to hold the result. */
void bytes_from_mpz(unsigned char*& outBuf, size_t* l, const mpz_class& n);

/* NOTE: the interface here uses a return value rather than a by ref
 * parameter on purpose: this is better for const initialization.
 * NOTE: this defaults to *big-endian* for the word order, in contrast
 * with the above functions that operate on bytes. */
mpz_class mpz_from_dwords(uint32_t* inBuf, size_t nWords, int endian=1);

/* use bytes_from_mpz to write n to a file descriptor in the following format:
 * 8 bytes for the length of n, followed by the bytes of n, produced
 * by the above function bytes_from_mpz */
void mpz_to_file(int fd, const mpz_class& n);

/* reads an integer from an open file descriptor.
 * uses the same format as mpz_to_file */
void mpz_from_file(int fd, mpz_class& n);
