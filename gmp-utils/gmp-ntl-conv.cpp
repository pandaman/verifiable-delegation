/* Some conversion routines for GMP <--> NTL,
 * as well as a few other utility functions... */

#include "gmp-utils.h"
#include <NTL/ZZ_p.h>
using namespace NTL;
#include <cassert>
#include <unistd.h> // for read, write

/* for convenience, and to save calls to malloc + free,
 * we set a max byte length for our integers. */
//#define Z_MAX_BYTES 256
#define MP_MAX_BYTES 256

void zztompz(mpz_class& r, const ZZ& n)
{
	size_t nBytes = NumBytes(n);
	assert(nBytes <= MP_MAX_BYTES);
	unsigned char bytes[MP_MAX_BYTES];
	BytesFromZZ(bytes,n,nBytes);
	mpz_from_bytes(r, bytes, nBytes);
}

void mpztozz(ZZ& r, const mpz_class& n)
{
	size_t nBytes;
	/* for this direction, we'll let the bytes_from_mpz function
	 * allocate memory for us. */
	unsigned char* bytes = 0;
	bytes_from_mpz(bytes,&nBytes,n);
	ZZFromBytes(r,bytes,nBytes);
	/* the bytes_from_mpz function above allocated memory, so we have
	 * to free it here. */
	free(bytes);
}

/* we'll try to use the same format here as we did for gmp:
 * 8 bytes for the length of n, followed by the bytes of n, produced
 * by the above function bytes_from_mpz */
void zz_to_file(int fd, const ZZ& n)
{
	size_t nBytes = NumBytes(n);
	assert(nBytes <= MP_MAX_BYTES);
	unsigned char bytes[MP_MAX_BYTES];
	BytesFromZZ(bytes,n,nBytes);
	write(fd,&nBytes,sizeof(size_t));
	write(fd,bytes,nBytes);
}

void zz_to_file(int fd, const ZZ_p& n)
{
	zz_to_file(fd,rep(n));
}

/* reads an integer from an open file descriptor.
 * uses the same format as mpz_to_file */
void zz_from_file(int fd, ZZ& n)
{
	unsigned char bytes[MP_MAX_BYTES];
	size_t nBytes = 0;
	read(fd,&nBytes,sizeof(size_t));
	assert(nBytes <= MP_MAX_BYTES);
	read(fd,bytes,nBytes);
	ZZFromBytes(n,bytes,nBytes);
}

void zz_from_file(int fd, ZZ_p& n)
{
	ZZ tmp;
	zz_from_file(fd,tmp);
	conv(n,tmp);
}

