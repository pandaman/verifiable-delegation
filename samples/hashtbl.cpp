/* test program demonstrating hash tables with sha1.
 * */

#include <cstdio>
#include <string.h>
#include <openssl/sha.h>
#include <unordered_set>
#include <string>
using std::string;
using std::unordered_set;

int main(int argc, char *argv[])
{
	typedef unordered_set<string> HS;
	HS S;
	/* as a test, we'll read our other source file. */
	FILE* f;
	f = fopen("hmac.cpp","rb");
	char* line = 0;
	char* token;
	size_t len;
	ssize_t read;
	char delims[] = " ,.:;_\n\r\t*-=()"; // adjust as necessary.
	unsigned char output[20]; // for hash output.
	while ((read = getline(&line,&len,f)) != -1) {
		token = strtok(line,delims);
		while(token) {
			S.insert(token);
			token = strtok(NULL,delims);
		}
	}
	free(line); // let go of the last line (others are freed by getline)
	fclose(f);

	/* now iterate through hash table, printing the words
	 * along with their hashes. */
	HS::iterator it;
	for (it=S.begin(); it!= S.end(); it++) {
		printf("%s\t\t",it->c_str());
		SHA1((const unsigned char*)it->c_str(),it->length(),output);
		for (size_t i = 0; i < 20; i++) {
			printf("%02x",output[i]);
		}
		printf("\n");
	}
	return 0;
}

