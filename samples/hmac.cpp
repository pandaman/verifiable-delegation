/* test program to demonstrate the usage of hash functions
 * and hmac from the openssl library.
 *
 * NOTE: to test the output, compare with the following:
 * $ echo -n "hello, world." | openssl sha1
 * $ echo -n "hello, world." | openssl dgst -sha1 -hmac "this is the key"
 * */

#include <cstdio>
#include <string.h>
#include <openssl/engine.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>

#define USECTX 1

int main(int argc, char *argv[])
{
	unsigned char testString[] = "hello, world.";
	unsigned char output[20];
	/* First test hashing: */
	SHA1(testString,strlen((char*)testString),output);
	for (size_t i = 0; i < 20; i++) {
		printf("%02x",output[i]);
	}
	printf("\n");

	/* now test hmac with sha1 */

	unsigned char key[] = "this is the key";
	unsigned int result_len = 20;
#ifdef USECTX
	HMAC_CTX ctx;
	unsigned char result[20];
	ENGINE_load_builtin_engines();
	ENGINE_register_all_complete();
	HMAC_CTX_init(&ctx);
#else
	unsigned char* result;
#endif

	for (size_t i=0; i < 1000000; i++)
	{
#ifdef USECTX
		HMAC_Init_ex(&ctx, key, strlen((char*)key), EVP_sha1(), NULL);
		HMAC_Update(&ctx, testString, strlen((char*)testString));
		HMAC_Final(&ctx, result, &result_len);
#else
		result = HMAC(EVP_sha1(),key,strlen((char*)key),
				testString,strlen((char*)testString),NULL,NULL);
#endif
	}

#ifdef USECTX
		HMAC_CTX_cleanup(&ctx);
#endif

	/* NOTE: You'll want to do this over and over.  I'm not
	 * sure how to reset the hmac context state, but I'm sure
	 * there is a way.  You don't want to kill the context and
	 * re-create it every time... */

	for (size_t i = 0; i < result_len; i++) {
		printf("%02x",result[i]);
	}
	
	//(stdin)= ff4479ce7b4daadec92d5c04cc1872083219be63
	//(stdin)= fc083753c736e5d67c733b8857e1b58538106fe3
	//         a1a6bb411bac18fc1d0c88eba5a841d0498ea6cf
	//         daeac1c55cd0ae838021cbb6b6cbd27126245ecb

	printf("\n");
	return 0;
}

