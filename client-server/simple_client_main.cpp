#include "ClientSocket.h"
#include "SocketException.h"
#include <iostream>
#include <string>
#include <cstdlib>

int main (int argc, char * argv[]){
  try{
      std::string reply;
      ClientSocket client_socket ("localhost", 30000);
      try{
          std::string message;
	  while (std::cin >> message){
               client_socket << message;
	       client_socket >> reply;
	       std::cout << reply << std::endl;
	  }
	}
      catch (SocketException& ){}
      std::cout << "We received this response from server:\n\n\n" << reply;
  }
  catch(SocketException& error){ std::cout << "EXCEPTION_ERROR. PLEASE REVIEW: :" << error.description(); }
  return 0;
}
