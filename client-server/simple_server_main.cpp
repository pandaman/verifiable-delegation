#include "ServerSocket.h"
#include "SocketException.h"
#include <string>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <stdio.h>

int main(int argc, char * argv[]){
  try{
      ServerSocket server ( 30000 );
      while (true){
	  ServerSocket new_sock;
	  server.accept (new_sock);
	  try{
	      while (true){
		  char * command_char;
		  std::string data;
		  new_sock >> data;
		  command_char = new char[data.size()+1];
		  strcpy(command_char, data.c_str());
		  system(command_char);
		  new_sock << "\nSIGNAL RECEIVED SUCCESSFULLY\n";
	      }
	  }
	  catch (SocketException& ){}
      }
  }
  catch (SocketException& error){ std::cout << "EXCEPTION_ERROR. PLEASE REVIEW: " << error.description() << std::endl; }
  return 0;
}
