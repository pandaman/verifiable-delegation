THIS IS A VERY SIMPLISTIC CLIENT-SERVER CONNECTION.
THERE HAS BEEN NO USE OF /CONSTRUCTION1 DIRECTORY
FILES DIRECTLY IN ORDER TO MAKE THINGS EASIER FOR NOW. 


The CLIENT sends a shell command to the SERVER. The
server executes the shell command (in this case the 
command should simply be):
	 ../Construction1/main 1
That command will simply instruct the server to 
run the main file in the Construction1 directory. 


***** HOW TO USE *******

1. cd into client-server directory and run "make". 
2. Open one more terminal window and cd the same
directory.
3. In one of the windows run:
	./simple-server
This will initiate the server and keep it into
"listening" mode. It will simply wait for any 
connection on port 30,000 and will act accordingly
to commands coming from that connection
4. In the other terminal window run:
	./simple-client
Now the client is connected to the server, however
nothing will happen until you send a command. To test
connection, simply write:
	../Construction1/main 1
The output of the client window should be:
	SIGNAL RECEIVED SUCCESSFULLY
The output of the server window should be the same as
the output of ./main 1 inside the Construction1 directory.


TO DO:::::::::::::
	1. Hide output of shell commands to server
	2. Transmit hidden server output to client and
	have client view it.
